Agent for the web services of the Central Bank of the Russian Federation
========================================================================
Getting daily data on exchange rates, stock indexes e t.c. via CBR web services

http://www.cbr.ru/scripts/Root.asp?PrtId=DWS

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist robote13/yii2-cbr-webservices "*"
```

or add

```
"robote13/yii2-cbr-webservices": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

#!php
```
$service = new \robote13\CBRwebservice\CBRServicesAPI(CBRService::SERVICE_DAYINFO); 
$xml = $service->getData('CursOnDate',['On_date'=>$date]));

```

Also, for access to the service method can be used implemented wrapper-methods :

#!php
```
$service = new \robote13\CBRwebservice\CBRServicesAPI(CBRService::SERVICE_DAYINFO); 
$xml = $service->exchangeRates();

```