<?php
/**
 * 
 */

namespace robote13\CBRwebservice;

/**
 * Description of CBRAgent
 *
 * @author Tartharia
 */
class CBRServicesAPI extends \SoapClient{
    
    const SERVICE_DAYINFO = 'http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL';
    
    const SERVICE_REGION_INFO = 'http://www.cbr.ru/RegionWebServ/regional.asmx?WSDL';

    /**
     * 
     * @param string $wsdl
     * @param array $options
     */
    public function __construct($wsdl, $options = []) {
        parent::SoapClient($wsdl,$options);
    }
    
    /**
     * 
     * @param string $operation_name operation name without prefix 'Get' and postfix 'XML'. List of operations {@link http://www.cbr.ru/scripts/Root.asp?PrtId=DWS}
     * @param array $arguments arguments list ['arg_name'=>'value'].
     * @return \DOMDocument
     */
    public function getData($operation_name,$arguments = [])
    {
        $document = new \DOMDocument();
        $document->loadXML($this->{"Get{$operation_name}XML"}($arguments)->{"Get{$operation_name}XMLResult"}->any);
        return $document;
    }
    
    /**
     * 
     * @param string $date textual datetime description. Valid formats are explained in {@link http://php.net/manual/en/datetime.formats.php}
     * @return \DOMDocument
     */
    public function exchangeRates($date = null)
    {
        $dateFormat = 'Y-m-d\TH:i:s';
        if (!isset($date)){
            $date = date($dateFormat);
        }else{
            $date = date($dateFormat,strtotime($date));
        }
        
        return $this->getData('CursOnDate',['On_date'=>$date]);
    }
}
